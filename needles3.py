import math
import random
import matplotlib.pyplot as plt

hit = 0
miss = 0
x = []
estimates = []
pi = []
cycles = 100
drops_per_cycle = 100000

print("##############################################################")
print("  Monte Carlo Similation of Buffon's needle estimation of pi ")
print()
print("  Number of simulated needle drops will be", cycles*drops_per_cycle)
print("##############################################################")
print()

for a in range(cycles):

    for b in range(drops_per_cycle):
        y = random.random()
        angle_deg = random.uniform(0,180)
        needle = math.sin(math.radians(angle_deg))
        if y + needle > 1 :
            hit += 1
        else :
            miss += 1
    current_estimate = 2.0*(hit+miss)/hit
    print (current_estimate, 'with', format(hit+miss, ',d'),'Needles thrown')
    x.append(hit+miss)
    estimates.append(current_estimate)
    pi.append(math.pi)
    #print(x)
    #print(y)
print ('===================================================')
print ('Final approximation of pi is:',2.0*(hit+miss)/hit)
print ('      Comapare to pi that is:',math.pi)
#plt.fill_between(x,pi,estimates)
plt.plot(x,estimates, label='estimate')
plt.plot(x,pi, label='pi')
plt.xlabel("Number of needles thrown")
plt.title("Monte Carlo Simulation of Buffon's Needles")
plt.legend()
plt.show()
        
