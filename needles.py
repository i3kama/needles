import math
import random

hit = 0
miss = 0


for a in range(100):

    for b in range(100000):
        y = random.random()
        angle_deg = random.uniform(0,180)
        needle = math.sin(math.radians(angle_deg))
        if y + needle > 1 :
            hit += 1
        else :
            miss += 1

    print 2.0*(hit+miss)/hit, 'with', format(hit+miss, ',d'),'Needles thrown'       
print '================='
print 'final approximation of pi is:',2.0*(hit+miss)/hit

        
